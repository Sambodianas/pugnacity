### BMD-Based Node Express Template Project

This project was built on an Express-based GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

### Development Notes

This project will be developed on the front end using [Bootstrap Material Design](https://fezvrasta.github.io/bootstrap-material-design/) &mdash; BMD and not to be mistaken for [MDB](https://mdbootstrap.com/) and its unpardonably cluttered documentation pages.

Improvements to the template can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

### CI or CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, I can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings. And so can you.

## Todos os Meus Todos
### Ou Seja, Quefazeres

I set up the Express JS router with very simple nesting to start.

On the front end I set up [the BMD drawer](https://fezvrasta.github.io/bootstrap-material-design/docs/4.0/material-design/drawers/) and saw that it was ugly and came with jerky transitions and so I set out to write better SCSS for this feature. The theme is American writers associated with the Ashcan School.

Hit <kbd>F12</kbd> and you do see responsive cardlike elements but also that the topmost of these overlays the top app bar.

```
.bmd-layout-container.bmd-drawer-f-r.bmd-drawer-overlay
  header.bmd-layout-header
    .navbar.navbar-light.bg-secondary.text-white.fixed-top
       button.navbar-toggler.text-white(type='button', data-toggle='drawer', data-target='#dw-s2')
    span.sr-only Toggle drawer
    i.material-icons menu
```

But this configuration does not fix the topbar topmost as it should.

Write `fixed-top` on `.header.bmd-layout-header` and it does. Now set `margin-top` on `<main>` and the remaining adjustment is padding or gutter on the cardlike elements &mdash; or better, flex spacing as in `layout-content: space-around`.

### Theming

### Advanced Layout

### Groovier Components
