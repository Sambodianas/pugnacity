var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/mark-twain', function(req, res, next) {
  res.render('ashcan-writing/mark-twain', { title: 'Humility-Pride' });
});

router.get('/horatio-alger', function(req, res, next) {
  res.render('ashcan-writing/horatio-alger', { title: 'Abstinence-Gluttony' });
});

router.get('/jacob-riis', function(req, res, next) {
  res.render('ashcan-writing/jacob-riis', { title: 'Chastity-Lust' });
});

router.get('/edith-wharton', function(req, res, next) {
  res.render('ashcan-writing/edith-wharton', { title: 'Patience-Anger' });
});

router.get('/liberality', function(req, res, next) {
  res.render('gindex', { title: 'Liberality-Greed' });
});

router.get('/diligence', function(req, res, next) {
  res.render('tindex', { title: 'Diligence-Sloth' });
});

module.exports = router;
